/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
    function getDetails(){
        let fullName = prompt("Enter your fullname: ");
        let age = prompt("Enter your age: ");
        let location = prompt("Enter your location: ");
        alert("Thank you for your input!");

        console.log("Hello, " + fullName);
        console.log("You are " + age + " years old.");
        console.log("You live in " + location);
    }
    getDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
    function printFaveBands(){
        let faveBands = ["1. Parokya ni Edgar", "2. Kamikazee", "3. Urbandub", "4. Hale", "5. Eraserheads"]
        for (i = 0; i < faveBands.length; i++){
            console.log(faveBands[i])
        }
    }
    printFaveBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
    function printFaveMovies(){
        let faveMovies = ["Lastikman", "Gagamboy", "Pedro Penduko", "Darna", "Captain Barbel"]
        console.log("1. " + faveMovies[0]);
        console.log("Rotten Tomatoes Rating: 100%");
        console.log("2. " + faveMovies[1]);
        console.log("Rotten Tomatoes Rating: 99%");
        console.log("3. " + faveMovies[2]);
        console.log("Rotten Tomatoes Rating: 98%");
        console.log("4. " + faveMovies[3]);
        console.log("Rotten Tomatoes Rating: 97%");
        console.log("5. " + faveMovies[4]);
        console.log("Rotten Tomatoes Rating: 96%");
    }
    printFaveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();


// console.log(friend1);
// console.log(friend2);